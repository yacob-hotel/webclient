import { createApp } from "vue";

import App from "./App.vue";
import router from "./router/index.js";
import axios from "@/plugins/axios";
import modal from "@/plugins/modal";

import VueDOMPurifyHTML from "vue-dompurify-html";
import i18n from "@/plugins/i18n";
import "@/helpers/rules";
import "./index.css";

const app = createApp(App)
  .use(router)
  .use(axios)
  .use(modal)
  .use(VueDOMPurifyHTML)
  .use(i18n)
  .mount("#app");
