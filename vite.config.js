import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import pages from "vite-plugin-pages";
import layout from "vite-plugin-vue-layouts";
import graphql from "@rollup/plugin-graphql";
import components from "vite-plugin-components";

import vueI18n from "@intlify/vite-plugin-vue-i18n";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  server:{
    port:9005,
  },
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
      "vue-i18n": "petite-vue-i18n/dist/vue-i18n.runtime.esm-bundler.js",
    },
  },
  plugins: [
    vue({
      script: {
        refSugar: true
      }
    }),
    graphql(),
    components(),
    pages(),
    layout(),
    vueI18n({
      include: path.resolve(__dirname, "./src/locales/**"),
    }),
  ],
});
